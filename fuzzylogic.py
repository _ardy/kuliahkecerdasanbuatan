'''
Latihan Logika Fuzzy
1. Fuzzification (derajat keanggotaan)
2. Interferance
3. DeFuzzification

Keterangan :
    - ska = Sensor Kanan
    - sdp = Sensor Depan
    - pwm = Pulse Width Modulation (kecepatan motor)

Derajat Keanggotaan SENSOR KANAN
    - sangat dekat  = (-15) - (-3)
    - dekat         = (-4) - (-1)
    - tepat         = (-2) - (3)
    - jauh          = 1 - 7
    - sangat jauh   = 5 - 21

Derajat Keanggotaan SENSOR DEPAN
    - dekat         = (-15) - 2
    - jauh          = (-2) - 7
    - sangat jauh   = 5 - 21

Derajat Keanggotaan PWM
    - slambat       = (-224) - (-32)
    - lambat        = (-160) - 32
    - nol           = (-96) - 96
    - sedang        = 32 - 224
    - cepat         = 160 - 352

'''

skaSDekatMin = -15
skaSDekatMax = -4

skaDekatMin = -4
skaDekatMax = -1

skaTepatMin = -2
skaTepatMax = 3

skaJauhMin = 1 
skaJauhMax = 7

skaSJauhMin = 5
skaSJauhMax = 21

sdpDekatMin = -15
sdpDekatMax = 2

sdpJauhMin = -2
sdpJauhMax = 7

sdpSJauhMin = 5
sdpSJauhMax = 21

pwmSLambatMin = -224
pwmSLambatMax = -32

pwmLambatMin = -160
pwmLambatMax = 32

pwmNolMin = -96
pwmNolMax = 96

pwmSedangMin = 32
pwmSedangMax = 224

pwmCepatMin = 160
pwmCepatMax = 352

def fungsiKeanggotaanSegitiga(a,b,c,x):
    if(x <= a):
        derajatKeanggotaan = 0
    elif((x >= a) and (x <= b)):
        derajatKeanggotaan = (x - a) / (b -a)
    elif((x >= b) and (x <= c)):
        derajatKeanggotaan = (c - x) / (c - b)
    else:
        derajatKeanggotaan = 0

    return derajatKeanggotaan

def fungsiKeanggotaanTrapesium(a,b,c,d,x):
    if(x <= a):
        derajatKeanggotaan = 0
    elif((x >= a) and (x <= b)):
        derajatKeanggotaan = (x - a) / (b -a)
    elif((x >= b) and (x <= c)):
        derajatKeanggotaan = 1
    elif((x >= c) and (x <= d)):
        derajatKeanggotaan = (d - x) / (d - c)
    else:
        derajatKeanggotaan = 0

    return derajatKeanggotaan

def inRange(minimal, maximal, input):
    minimal = min(minimal, maximal)
    maximal = max(minimal, maximal)

    if((input > minimal) and (input < maximal)):
        return 1
    else:
        return 0

def derajatSka(ska):
    global derajatSkaSDekat
    global derajatSkaDekat
    global derajatSkaTepat
    global derajatSkaJauh
    global derajatSkaSJauh

    derajatSkaSDekat = fungsiKeanggotaanTrapesium(skaSDekatMin, -10, -5, skaSDekatMax, ska)
    derajatSkaDekat = fungsiKeanggotaanSegitiga(skaDekatMin, -3, skaDekatMax, ska)
    derajatSkaTepat = fungsiKeanggotaanSegitiga(skaTepatMin, 0, skaTepatMax, ska)
    derajatSkaJauh = fungsiKeanggotaanSegitiga(skaJauhMin, 4, skaJauhMax, ska)
    derajatSkaSJauh = fungsiKeanggotaanTrapesium(skaSJauhMin, 8, 18, skaSJauhMax, ska)

    print ("Derajat Sensor Kanan Sangat Dekat : ", derajatSkaSDekat)
    print ("Derajat Sensor Kanan Dekat : ", derajatSkaDekat)
    print ("Derajat Sensor Kanan Tepat : ", derajatSkaTepat)
    print ("Derajat Sensor Kanan Jauh : ", derajatSkaJauh)
    print ("Derajat Sensor Kanan Sangat Jauh : ", derajatSkaSJauh)

def derajatSdp(sdp):
    global derajatSdpDekat
    global derajatSdpJauh
    global derajatSdpSJauh

    derajatSdpDekat = fungsiKeanggotaanTrapesium(sdpDekatMin, -10, -5, sdpDekatMax, sdp)
    derajatSdpJauh = fungsiKeanggotaanSegitiga(sdpJauhMin, 4, sdpJauhMax, sdp)
    derajatSdpSJauh = fungsiKeanggotaanTrapesium(sdpSJauhMin, 8, 18, sdpSJauhMax, sdp)

    print ("Derajat Sensor Depan Dekat : ", derajatSdpDekat)
    print ("Derajat Sensor Depan Jauh : ", derajatSdpJauh)
    print ("Derajat Sensor Depan Sangat Jauh : ", derajatSdpSJauh)

def fuzzyRules(ska, sdp):
    global nPWMNol
    global nPWMSedang
    global nPWMCepat
    global nPWMLambat

    nPWMNol = 0
    nPWMSedang = 0
    nPWMCepat = 0
    nPWMLambat = 0

    skaSDekat = inRange(skaSDekatMin, skaSDekatMax, ska)
    skaDekat = inRange(skaDekatMin, skaDekatMax, ska)
    skaTepat = inRange(skaTepatMin, skaTepatMax, ska)
    skaJauh = inRange(skaJauhMin, skaJauhMax, ska)
    skaSJauh = inRange(skaSJauhMin, skaSJauhMax, ska)

    sdpDekat = inRange(sdpDekatMin, sdpDekatMax, sdp)
    sdpJauh = inRange(sdpJauhMin, sdpJauhMax, sdp)
    sdpSJauh = inRange(sdpSJauhMin, sdpSJauhMax, sdp)

    pwmCepat = []
    pwmSedang = []
    pwmNol = []
    pwmLambat = []
    pwmSLambat = []

    if skaSDekat == 1 and sdpDekat == 1:
        print ("Rule 1 : Sensor Kanan Sangat Dekat dan Sensor Depan Dekat")
        derajatPWMCepat = min(derajatSkaSDekat, derajatSdpDekat)
        print ("Derajat PWM Cepat ", derajatPWMCepat)
        nPWMCepat = derajatPWMCepat
        pwmCepat.append(nPWMCepat)
    
    if skaSDekat == 1 and sdpJauh == 1:
        print ("Rule 2 : Sensor Kanan Sangat Dekat dan Sensor Depan Jauh")
        derajatPWMSedang = min(derajatSkaSDekat, derajatSdpJauh)
        print ("Derajat PWM Sedang ", derajatPWMSedang)
        nPWMSedang = derajatPWMSedang
        pwmSedang.append(nPWMSedang)

    if skaSDekat == 1 and sdpSJauh == 1:
        print ("Rule 3 : Sensor Kanan Sangat Dekat dan Sensor Depan Sangat Jauh")
        derajatPWMCepat = min(derajatSkaSDekat, derajatSdpSJauh)
        print ("Derajat PWM Cepat ", derajatPWMCepat)
        nPWMCepat = derajatPWMCepat
        pwmCepat.append(nPWMCepat)

    ###

    if skaDekat == 1 and sdpDekat == 1:
        print ("Rule 4 : Sensor Kanan Dekat dan Sensor Depan Dekat")
        derajatPWMCepat = min(derajatSkaDekat, derajatSdpDekat)
        print ("Derajat PWM Cepat ", derajatPWMCepat)
        nPWMCepat = derajatPWMCepat
        pwmCepat.append(nPWMCepat)
    
    if skaDekat == 1 and sdpJauh == 1:
        print ("Rule 5 : Sensor Kanan Dekat dan Sensor Depan Jauh")
        derajatPWMCepat = min(derajatSkaDekat, derajatSdpJauh)
        print ("Derajat PWM Cepat ", derajatPWMCepat)
        nPWMCepat = derajatPWMCepat
        pwmCepat.append(nPWMCepat)

    if skaDekat == 1 and sdpSJauh == 1:
        print ("Rule 6 : Sensor Kanan Sangat Dekat dan Sensor Depan Sangat Jauh")
        derajatPWMCepat = min(derajatSkaDekat, derajatSdpSJauh)
        print ("Derajat PWM Cepat ", derajatPWMCepat)
        nPWMCepat = derajatPWMCepat
        pwmCepat.append(nPWMCepat)

    ###

    if skaTepat == 1 and sdpDekat == 1:
        print ("Rule 7 : Sensor Kanan Tepat dan Sensor Depan Dekat")
        derajatPWMCepat = min(derajatSkaTepat, derajatSdpDekat)
        print ("Derajat PWM Cepat ", derajatPWMCepat)
        nPWMCepat = derajatPWMCepat
        pwmCepat.append(nPWMCepat)
    
    if skaTepat == 1 and sdpJauh == 1:
        print ("Rule 8 : Sensor Kanan Tepat dan Sensor Depan Jauh")
        derajatPWMSedang = min(derajatSkaTepat, derajatSdpJauh)
        print ("Derajat PWM Sedang ", derajatPWMSedang)
        nPWMSedang = derajatPWMSedang
        pwmSedang.append(nPWMSedang)

    if skaTepat == 1 and sdpSJauh == 1:
        print ("Rule 9 : Sensor Kanan Tepat dan Sensor Depan Sangat Jauh")
        derajatPWMCepat = min(derajatSkaTepat, derajatSdpSJauh)
        print ("Derajat PWM Cepat ", derajatPWMCepat)
        nPWMCepat = derajatPWMCepat
        pwmCepat.append(nPWMCepat)

    ###

    if skaJauh == 1 and sdpDekat == 1:
        print ("Rule 10 : Sensor Kanan Jauh dan Sensor Depan Dekat")
        derajatPWMCepat = min(derajatSkaJauh, derajatSdpDekat)
        print ("Derajat PWM Cepat ", derajatPWMCepat)
        nPWMCepat = derajatPWMCepat
        pwmCepat.append(nPWMCepat)
    
    if skaJauh == 1 and sdpJauh == 1:
        print ("Rule 11 : Sensor Kanan Jauh dan Sensor Depan Jauh")
        derajatPWMNol = min(derajatSkaJauh, derajatSdpJauh)
        print ("Derajat PWM Nol ", derajatPWMNol)
        nPWMNol = derajatPWMNol
        pwmNol.append(nPWMNol)

    if skaJauh == 1 and sdpSJauh == 1:
        print ("Rule 12 : Sensor Kanan Jauh dan Sensor Depan Sangat Jauh")
        derajatPWMSedang = min(derajatSkaJauh, derajatSdpSJauh)
        print ("Derajat PWM Sedang ", derajatPWMSedang)
        nPWMSedang = derajatPWMSedang
        pwmSedang.append(nPWMSedang)

    ###

    if skaSJauh == 1 and sdpDekat == 1:
        print ("Rule 13 : Sensor Kanan Sangat Jauh dan Sensor Depan Dekat")
        derajatPWMCepat = min(derajatSkaSJauh, derajatSdpDekat)
        print ("Derajat PWM Cepat ", derajatPWMCepat)
        nPWMCepat = derajatPWMCepat
        pwmCepat.append(nPWMCepat)
    
    if skaSJauh == 1 and sdpJauh == 1:
        print ("Rule 14 : Sensor Kanan Sangat Jauh dan Sensor Depan Jauh")
        derajatPWMLambat = min(derajatSkaSJauh, derajatSdpJauh)
        print ("Derajat PWM Nol ", derajatPWMLambat)
        nPWMLambat = derajatPWMLambat
        pwmLambat.append(nPWMLambat)

    if skaSJauh == 1 and sdpSJauh == 1:
        print ("Rule 15 : Sensor Kanan Sangat Jauh dan Sensor Depan Sangat Jauh")
        derajatPWMNol = min(derajatSkaSJauh, derajatSdpSJauh)
        print ("Derajat PWM Nol ", derajatPWMNol)
        nPWMNol = derajatPWMNol
        pwmNol.append(nPWMNol)


    if nPWMNol == 0:
        nPWMNol = 0
    else:
        nPWMNol = max(pwmNol)

    if nPWMSedang == 0:
        nPWMSedang = 0
    else:
        nPWMSedang = max(pwmSedang)

    if nPWMCepat == 0:
        nPWMCepat = 0
    else:
        nPWMCepat = max(pwmCepat)

    if nPWMLambat == 0:
        nPWMLambat = 0
    else:
        nPWMLambat = max(pwmLambat)

    print ("PWM Nol : ", pwmNol)
    print ("PWM Sedang : ", pwmSedang)
    print ("PWM Cepat : ", pwmCepat)
    print ("PWM Lambat : ", pwmLambat)

def centroidMethod(nPWMNol, nPWMSedang, nPWMCepat, nPWMLambat):
    global hasil

    hasil =  (((-96)+(-64)+(-32)+0+32+64+96)*nPWMNol+(32+64+96+128+160+192+224)*nPWMSedang+(160+192+224+256+288+320+352)*nPWMCepat+((-160)+(-128)+(-96)+(-64)+(-32)+32)*nPWMLambat) / ((1 * nPWMNol)+(3 * nPWMSedang)+(8 * nPWMCepat)+(1 * nPWMLambat))

    print("PWM : ", hasil)
    return hasil

def main() :
    data = []

    ska = float(input("Masukkan nilai error Sensor Kanan : "))
    sdp = float(input("Masukkan nilai error Sensor Depan : "))

    print ("FUZZIFICATION")
    dSka = derajatSka(ska)
    dSdp = derajatSdp(sdp)

    fuzzyRules(ska, sdp)

    defuzzyfication = centroidMethod(nPWMNol, nPWMSedang, nPWMCepat, nPWMLambat)
    print (defuzzyfication)
    defuzzy = data.append(defuzzyfication)

    print (data)

    return 0

if __name__=="__main__":
    main()